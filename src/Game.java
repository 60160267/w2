
public class Game {

	private Board board;
	private Player x;
	private Player o;

	Game() {
		o = new Player('O');
		x = new Player('X');
		board = new Board();
	}

	static void Play() {
		showWelcome();
		showTable();
		showTurn();
	}

	static void showWelcome() {
		System.out.println("Welcome To OX Game");
	}
	static void showTable() {
		System.out.println("  1 2 3");
		char[][] table = Board.getTable();
		for (int i = 0 ; i < 3 ; i++) {
			System.out.print(i+1 + " ");
			for (int j = 0 ; j < 3 ; j++) {
				System.out.print(table[i][j]+" ");
			}
			System.out.println();
		}
	}
	
	static void showTurn() {
		System.out.println(Board.getCurrent().getName()+" Turn...");
	}

}
