
public class Board {

	private static char table[][] = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
	private static Player x;
	private static Player o;
	private static Player winer;
	private static Player current;
	private static int turnCount;
	
	Board (){
		o = new Player('O');
		x = new Player('X');
		current = x;
		winer = null;
		turnCount = 0;
	}
	
	static char[][] getTable(){
		return table;
	}
	
	static Player getCurrent() {
		return current;
	}
	
	
}
