import java.util.*;

public class XO {

	static String[][] table = { { "-", "-", "-" }, { "-", "-", "-" }, { "-", "-", "-" } };

	public static void ShowTable() {
		System.out.println("  1 2 3");
		for (int i = 0; i < 3; i++) {
			System.out.print(i + 1 + " ");
			for (int j = 0; j < 3; j++) {
				System.out.print(table[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static void ShowWelcome() {
		System.out.println("Welcome to XO Game.");
	}

	public static void ShowSpace() {
		System.out.println();
	}

	public static boolean CheckDraw(int va) {
		if (va == 10) {
			System.out.println("Draw");
			return true;
		} else {
			return false;
		}
	}

	public static void ShowTurn(int va) {
		if (va % 2 != 0) {
			System.out.println("X turn");
		} else {
			System.out.println("O turn");
		}

	}

	public static boolean Input(int va) {
		Scanner key = new Scanner(System.in);
		System.out.print("Input Row Col : ");
		int Row = key.nextInt() - 1;
		int Col = key.nextInt() - 1;

		if (!(Row >= 0 && Row <= 2 && Col >= 0 && Col <= 2)) {
			System.out.println("Can't input.");
			ShowSpace();
			return false;
		}

		if (va % 2 != 0) {
			if (table[Row][Col] == "-" && Row >= 0 && Row <= 2 && Col >= 0 && Col <= 2) {
				table[Row][Col] = "x";
				return true;
			} else {
				System.out.println("Can't input.");
				ShowSpace();
				return false;
			}
		} else {
			if (table[Row][Col] == "-" && Row >= 0 && Row <= 2 && Col >= 0 && Col <= 2) {
				table[Row][Col] = "o";
				return true;
			} else {

				System.out.println("Can't input.");
				ShowSpace();
				return false;
			}
		}

	}

	public static boolean CheckWin() {
		if (table[0][0].equals("x") & table[0][1].equals("x") & table[0][2].equals("x")) {
			System.out.println("X Win!!");
			return true;
		} else if (table[1][0].equals("x") & table[1][1].equals("x") & table[1][2].equals("x")) {
			System.out.println("X Win!!");
			return true;
		} else if (table[2][0].equals("x") & table[2][1].equals("x") & table[2][2].equals("x")) {
			System.out.println("X Win!!");
			return true;
		} else if (table[0][0].equals("x") & table[1][0].equals("x") & table[2][0].equals("x")) {
			System.out.println("X Win!!");
			return true;
		} else if (table[0][1].equals("x") & table[1][1].equals("x") & table[2][1].equals("x")) {
			System.out.println("X Win!!");
			return true;
		} else if (table[0][2].equals("x") & table[1][2].equals("x") & table[2][2].equals("x")) {
			System.out.println("X Win!!");
			return true;
		} else if (table[0][0].equals("x") & table[1][1].equals("x") & table[2][2].equals("x")) {
			System.out.println("X Win!!");
			return true;
		} else if (table[0][2].equals("x") & table[1][1].equals("x") & table[2][0].equals("x")) {
			System.out.println("X Win!!");
			return true;
		} else if (table[0][0].equals("o") & table[0][1].equals("o") & table[0][2].equals("o")) {
			System.out.println("O Win!!");
			return true;
		} else if (table[1][0].equals("o") & table[1][1].equals("o") & table[1][2].equals("o")) {
			System.out.println("O Win!!");
			return true;
		} else if (table[2][0].equals("o") & table[2][1].equals("o") & table[2][2].equals("o")) {
			System.out.println("O Win!!");
			return true;
		} else if (table[0][0].equals("o") & table[1][0].equals("o") & table[2][0].equals("o")) {
			System.out.println("O Win!!");
			return true;
		} else if (table[0][1].equals("o") & table[1][1].equals("o") & table[2][1].equals("o")) {
			System.out.println("O Win!!");
			return true;
		} else if (table[0][2].equals("o") & table[1][2].equals("o") & table[2][2].equals("o")) {
			System.out.println("O Win!!");
			return true;
		} else if (table[0][0].equals("o") & table[1][1].equals("o") & table[2][2].equals("o")) {
			System.out.println("O Win!!");
			return true;
		} else if (table[0][2].equals("o") & table[1][1].equals("o") & table[2][0].equals("o")) {
			System.out.println("O Win!!");
			return true;
		} else {
			return false;
		}
	}

	public static void main(String[] args) {
		Scanner key = new Scanner(System.in);
		int round = 1;
		ShowWelcome();
		ShowTable();

		while (true) {

			if (round % 2 != 0) {
				ShowTurn(round);
				if (Input(round) == true) {
					
					round++;
					ShowSpace();
					ShowTable();
					if (CheckWin() == true) {
						break;
					}
					if (CheckDraw(round) == true) {
						break;
					}
				} 
			} else if (round % 2 == 0) {
				ShowTurn(round);
				if (Input(round) == true) {
					round++;
					ShowSpace();
					ShowTable();
					if (CheckWin() == true) {
						break;
					}
					if (CheckDraw(round) == true) {
						break;
					}

				} 
			}

		}

	}

}
