
public class Player {

	private static char name;
	private static int win;
	private static int draw;
	private static int lose;

	Player (char name ){
		this.name = name;

	}
	
	static char getName() {
		return name;
	}
	
	static int getWin() {
		return win;
	}
	
	static int getDraw() {
		return draw;
	}
	
	static int getLose() {
		return lose;
	}
	
	
	

}
